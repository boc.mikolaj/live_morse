/**
 * This file is part of the Live Morse project distribution (https://gitlab.com/boc.mikolaj/live_morse/).
 * Copyright (c) 2022 Mikolaj Boc.
 *
 * This file is part of the Live Morse project (LM).
 *
 * LM is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * LM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with LM. If not, see <https://www.gnu.org/licenses/>.
 */

#include "./translationscheduler.h"

#include <QtConcurrent>

#include "./translationengine.h"
#include "./translationenginetypes.h"

TranslationScheduler::TranslationScheduler(
        TranslationFunction translate, QObject *parent)
    : QObject{parent}, translate_(std::move(translate)) {
    translationWatcher_ = new QFutureWatcher<TranslationJob>(this);

    QObject::connect(translationWatcher_,
                     &QFutureWatcher<TranslationJob>::finished,
                     this,
                     &TranslationScheduler::onJobDone);
}

TranslationScheduler::TranslationJob::TranslationJob(
        int jobId, QString translatedText, TextEncoding resultEncoding)
    : jobId(jobId),
      translatedText(std::move(translatedText)),
      resultEncoding(resultEncoding) {}

TranslationScheduler::TranslationJob::~TranslationJob() = default;

TranslationScheduler::TranslationJob::
TranslationJob(TranslationJob&& other) = default;

TranslationScheduler::TranslationJob&
TranslationScheduler::TranslationJob::operator=(TranslationJob&& other)
    = default;

void TranslationScheduler::translate(
        QString originalText, TextEncoding sourceEncoding) {
    translationWatcher_->setFuture(
        QtConcurrent::run(
            [jobNumber = currentJobNumber_,
            translate = this->translate_,
            sourceEncoding]
            (QString source) {
                return TranslationJob(
                            jobNumber,
                            translate(std::move(source), sourceEncoding),
                            sourceEncoding == TextEncoding::Ascii ?
                                TextEncoding::Morse : TextEncoding::Ascii);
            }, std::move(originalText)));
}

void TranslationScheduler::onJobDone() {
    auto result = translationWatcher_->future().takeResult();
    emit translationReady(
                std::move(result.translatedText),
                result.resultEncoding);
}
