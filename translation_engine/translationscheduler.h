/**
 * This file is part of the Live Morse project distribution (https://gitlab.com/boc.mikolaj/live_morse/).
 * Copyright (c) 2022 Mikolaj Boc.
 *
 * This file is part of the Live Morse project (LM).
 *
 * LM is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * LM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with LM. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TRANSLATIONSCHEDULER_H
#define TRANSLATIONSCHEDULER_H

#include <functional>
#include <memory>

#include <QFutureWatcher>
#include <QObject>

#include "translation_engine_global.h"
#include "translationengine.h"

enum class TextEncoding;

// Allows for scheduling of potentially time-consuming translation tasks.
class TRANSLATION_ENGINE_EXPORT TranslationScheduler final : public QObject
{
    Q_OBJECT
public:
    using TranslationFunction = std::function<QString(QString, TextEncoding)>;

    TranslationScheduler(TranslationFunction translationFunction,
                         QObject *parent = nullptr);

    // Schedules a task to translate the |originalText| to the encoding
    // complementary to |sourceEncoding|. Depending on the state of the global
    // thread pool and input size, this might finish much later.
    // The |translationReady| signal is fired when the translation is ready.
    void translate(QString originalText, TextEncoding sourceEncoding);

signals:
    // See |translate|.
    void translationReady(
            QString translationText, TextEncoding translationEncoding);

private:
    struct TranslationJob {
        TranslationJob(int jobId,
                       QString translatedText,
                       TextEncoding resultEncoding);
        ~TranslationJob();

        TranslationJob(const TranslationJob& other) = delete;
        TranslationJob(TranslationJob&& other);
        TranslationJob& operator=(const TranslationJob& other) = delete;
        TranslationJob& operator=(TranslationJob&& other);

        int jobId;
        QString translatedText;
        TextEncoding resultEncoding;
    };

    int currentJobNumber_{0};
    QFutureWatcher<TranslationJob>* translationWatcher_;
    TranslationFunction translate_;

private slots:
    void onJobDone();
};

#endif // TRANSLATIONSCHEDULER_H
