/**
 * This file is part of the Live Morse project distribution (https://gitlab.com/boc.mikolaj/live_morse/).
 * Copyright (c) 2022 Mikolaj Boc.
 *
 * This file is part of the Live Morse project (LM).
 *
 * LM is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * LM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with LM. If not, see <https://www.gnu.org/licenses/>.
 */

#include "./translationengine.h"

#include <QHash>
#include <QRegularExpression>
#include <QStringLiteral>
#include <QStringTokenizer>

#include "./translationenginetypes.h"

namespace {
const QHash<QStringView, QStringView>& getSymbolMap(
        TextEncoding sourceEncoding) {
    static QString aMorse = QStringLiteral(".-");
    static QString bMorse = QStringLiteral("-...");
    static QString cMorse = QStringLiteral("-.-.");
    static QString dMorse = QStringLiteral("-..");
    static QString eMorse = QStringLiteral(".");
    static QString fMorse = QStringLiteral("..-.");
    static QString gMorse = QStringLiteral("--.");
    static QString hMorse = QStringLiteral("....");
    static QString iMorse = QStringLiteral("..");
    static QString jMorse = QStringLiteral(".---");
    static QString kMorse = QStringLiteral("-.-");
    static QString lMorse = QStringLiteral(".-..");
    static QString mMorse = QStringLiteral("--");
    static QString nMorse = QStringLiteral("-.");
    static QString oMorse = QStringLiteral("---");
    static QString pMorse = QStringLiteral(".--.");
    static QString qMorse = QStringLiteral("--.-");
    static QString rMorse = QStringLiteral(".-.");
    static QString sMorse = QStringLiteral("...");
    static QString tMorse = QStringLiteral("-");
    static QString uMorse = QStringLiteral("..-");
    static QString vMorse = QStringLiteral("...-");
    static QString wMorse = QStringLiteral(".--");
    static QString xMorse = QStringLiteral("-..-");
    static QString yMorse = QStringLiteral("-.--");
    static QString zMorse = QStringLiteral("--..");
    static QString d0Morse = QStringLiteral("-----");
    static QString d1Morse = QStringLiteral(".----");
    static QString d2Morse = QStringLiteral("..---");
    static QString d3Morse = QStringLiteral("...--");
    static QString d4Morse = QStringLiteral("....-");
    static QString d5Morse = QStringLiteral(".....");
    static QString d6Morse = QStringLiteral("-....");
    static QString d7Morse = QStringLiteral("--...");
    static QString d8Morse = QStringLiteral("---..");
    static QString d9Morse = QStringLiteral("----.");
    static QString spaceMorse = QStringLiteral("|");

    static QString aAscii = QStringLiteral("a");
    static QString bAscii = QStringLiteral("b");
    static QString cAscii = QStringLiteral("c");
    static QString dAscii = QStringLiteral("d");
    static QString eAscii = QStringLiteral("e");
    static QString fAscii = QStringLiteral("f");
    static QString gAscii = QStringLiteral("g");
    static QString hAscii = QStringLiteral("h");
    static QString iAscii = QStringLiteral("i");
    static QString jAscii = QStringLiteral("j");
    static QString kAscii = QStringLiteral("k");
    static QString lAscii = QStringLiteral("l");
    static QString mAscii = QStringLiteral("m");
    static QString nAscii = QStringLiteral("n");
    static QString oAscii = QStringLiteral("o");
    static QString pAscii = QStringLiteral("p");
    static QString qAscii = QStringLiteral("q");
    static QString rAscii = QStringLiteral("r");
    static QString sAscii = QStringLiteral("s");
    static QString tAscii = QStringLiteral("t");
    static QString uAscii = QStringLiteral("u");
    static QString vAscii = QStringLiteral("v");
    static QString wAscii = QStringLiteral("w");
    static QString xAscii = QStringLiteral("x");
    static QString yAscii = QStringLiteral("y");
    static QString zAscii = QStringLiteral("z");
    static QString d0Ascii = QStringLiteral("0");
    static QString d1Ascii = QStringLiteral("1");
    static QString d2Ascii = QStringLiteral("2");
    static QString d3Ascii = QStringLiteral("3");
    static QString d4Ascii = QStringLiteral("4");
    static QString d5Ascii = QStringLiteral("5");
    static QString d6Ascii = QStringLiteral("6");
    static QString d7Ascii = QStringLiteral("7");
    static QString d8Ascii = QStringLiteral("8");
    static QString d9Ascii = QStringLiteral("9");
    static QString spaceAscii = QStringLiteral(" ");

#define MAPPING(source, target)                       \
    {QStringView((source).cbegin(), (source).cend()), \
    QStringView((target).cbegin(), (target).cend())}

    switch (sourceEncoding) {
        case TextEncoding::Morse: {
            static QHash<QStringView, QStringView> symbolMap {
                MAPPING(aMorse, aAscii),
                MAPPING(bMorse, bAscii),
                MAPPING(cMorse, cAscii),
                MAPPING(dMorse, dAscii),
                MAPPING(eMorse, eAscii),
                MAPPING(fMorse, fAscii),
                MAPPING(gMorse, gAscii),
                MAPPING(hMorse, hAscii),
                MAPPING(iMorse, iAscii),
                MAPPING(jMorse, jAscii),
                MAPPING(kMorse, kAscii),
                MAPPING(lMorse, lAscii),
                MAPPING(mMorse, mAscii),
                MAPPING(nMorse, nAscii),
                MAPPING(oMorse, oAscii),
                MAPPING(pMorse, pAscii),
                MAPPING(qMorse, qAscii),
                MAPPING(rMorse, rAscii),
                MAPPING(sMorse, sAscii),
                MAPPING(tMorse, tAscii),
                MAPPING(uMorse, uAscii),
                MAPPING(vMorse, vAscii),
                MAPPING(wMorse, wAscii),
                MAPPING(xMorse, xAscii),
                MAPPING(yMorse, yAscii),
                MAPPING(zMorse, zAscii),
                MAPPING(d0Morse, d0Ascii),
                MAPPING(d1Morse, d1Ascii),
                MAPPING(d2Morse, d2Ascii),
                MAPPING(d3Morse, d3Ascii),
                MAPPING(d4Morse, d4Ascii),
                MAPPING(d5Morse, d5Ascii),
                MAPPING(d6Morse, d6Ascii),
                MAPPING(d7Morse, d7Ascii),
                MAPPING(d8Morse, d8Ascii),
                MAPPING(d9Morse, d9Ascii),
                MAPPING(spaceMorse, spaceAscii),
            };
            return symbolMap;
        }
        case TextEncoding::Ascii: {
            static QHash<QStringView, QStringView> symbolMap {
                MAPPING(aAscii, aMorse),
                MAPPING(bAscii, bMorse),
                MAPPING(cAscii, cMorse),
                MAPPING(dAscii, dMorse),
                MAPPING(eAscii, eMorse),
                MAPPING(fAscii, fMorse),
                MAPPING(gAscii, gMorse),
                MAPPING(hAscii, hMorse),
                MAPPING(iAscii, iMorse),
                MAPPING(jAscii, jMorse),
                MAPPING(kAscii, kMorse),
                MAPPING(lAscii, lMorse),
                MAPPING(mAscii, mMorse),
                MAPPING(nAscii, nMorse),
                MAPPING(oAscii, oMorse),
                MAPPING(pAscii, pMorse),
                MAPPING(qAscii, qMorse),
                MAPPING(rAscii, rMorse),
                MAPPING(sAscii, sMorse),
                MAPPING(tAscii, tMorse),
                MAPPING(uAscii, uMorse),
                MAPPING(vAscii, vMorse),
                MAPPING(wAscii, wMorse),
                MAPPING(xAscii, xMorse),
                MAPPING(yAscii, yMorse),
                MAPPING(zAscii, zMorse),
                MAPPING(d0Ascii, d0Morse),
                MAPPING(d1Ascii, d1Morse),
                MAPPING(d2Ascii, d2Morse),
                MAPPING(d3Ascii, d3Morse),
                MAPPING(d4Ascii, d4Morse),
                MAPPING(d5Ascii, d5Morse),
                MAPPING(d6Ascii, d6Morse),
                MAPPING(d7Ascii, d7Morse),
                MAPPING(d8Ascii, d8Morse),
                MAPPING(d9Ascii, d9Morse),
                MAPPING(spaceAscii, spaceMorse),
            };
            return symbolMap;
        }
    }

#undef MAPPING
}
}  // namespace

QString translate(QString source, TextEncoding sourceEncoding) {
    const auto& symbolMap = getSymbolMap(sourceEncoding);

    QRegularExpression re{R"reg(\s+)reg"};
    source = source.replace(re, " ");

    QString output;
    if (sourceEncoding == TextEncoding::Morse) {
        QStringTokenizer tokenizer{source, QString(" "), Qt::SkipEmptyParts};

        for (auto token : tokenizer) {
            // Should be fast according to documentation for QString
            const auto it = symbolMap.find(token);
            output += it != symbolMap.end() ? *it : token;
        }
        return output;
    }
    source = source.toLower().trimmed();
    for (auto ch = source.cbegin(); ch != source.cend(); ++ch) {
        const auto it = symbolMap.find(QStringView(&*ch, 1));
        output += (it != symbolMap.end() ? *it : QStringView(&*ch, 1));
        if (ch != source.cend() - 1)
            output += " ";
    }
    return output;
}
