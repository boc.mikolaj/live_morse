# This file is part of the Live Morse project distribution (https://gitlab.com/boc.mikolaj/live_morse/).
# Copyright (c) 2022 Mikolaj Boc.
#
# This file is part of the Live Morse project (LM).
#
# LM is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# LM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with LM. If not, see <https://www.gnu.org/licenses/>.

QT -= gui
QT += concurrent

TEMPLATE = lib
DEFINES += TRANSLATION_ENGINE_LIBRARY

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    translationengine.cpp \
    translationscheduler.cpp

HEADERS += \
    translation_engine_global.h \
    translationengine.h \
    translationenginetypes.h \
    translationscheduler.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
