/**
 * This file is part of the Live Morse project distribution (https://gitlab.com/boc.mikolaj/live_morse/).
 * Copyright (c) 2022 Mikolaj Boc.
 *
 * This file is part of the Live Morse project (LM).
 *
 * LM is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * LM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with LM. If not, see <https://www.gnu.org/licenses/>.
 */

#include "./livemorsewindow.h"

#include <utility>

#include <QFile>
#include <QSignalMapper>

#include "./ui_livemorsewindow.h"
#include "./ui_dialog.h"
#include "../translation_engine/translationengine.h"
#include "../translation_engine/translationenginetypes.h"
#include "../translation_engine/translationscheduler.h"

LiveMorseWindow::LiveMorseWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui_(std::make_unique<Ui::LiveMorseWindow>()) {
    ui_->setupUi(this);
    ui_->morseTextEdit->installEventFilter(this);
    ui_->latinTextEdit->installEventFilter(this);

    editWithFocus_ = ui_->morseTextEdit;

    translationScheduler_ =
            new TranslationScheduler(&translate, this);

    signalMapper_ = new QSignalMapper(this);
    QObject::connect(ui_->morseTextEdit, SIGNAL(textChanged()),
                     signalMapper_, SLOT(map()));
    signalMapper_->setMapping(
                ui_->morseTextEdit, static_cast<int>(TextEncoding::Morse));

    QObject::connect(ui_->latinTextEdit, SIGNAL(textChanged()),
                     signalMapper_, SLOT(map()));
    signalMapper_->setMapping(
                ui_->latinTextEdit, static_cast<int>(TextEncoding::Ascii));

    QObject::connect(signalMapper_, SIGNAL(mappedInt(int)),
                     this, SLOT(onTextChangedMapped(int)));

    QObject::connect(translationScheduler_,
                     SIGNAL(translationReady(QString, TextEncoding)),
                     this,
                     SLOT(onTranslationReady(QString, TextEncoding)));

    QObject::connect(ui_->actionQuit, SIGNAL(triggered()),
                     this, SLOT(close()));

    QObject::connect(ui_->actionLicense, SIGNAL(triggered(bool)),
                     this, SLOT(onLicenseTriggered(bool)));
}

LiveMorseWindow::~LiveMorseWindow() = default;

void LiveMorseWindow::onTextChangedMapped(int param) {
    onTextChanged(static_cast<TextEncoding>(param));
}

bool LiveMorseWindow::eventFilter(QObject *object, QEvent *event) {
    Q_ASSERT(object == ui_->latinTextEdit || object == ui_->morseTextEdit);

    if (event->type() != QEvent::FocusIn)
        return false;

    editWithFocus_ = static_cast<QTextEdit*>(object);
    return false;
}

void LiveMorseWindow::onTextChanged(TextEncoding textEditEncoding) {
    const QTextEdit* source = textEditEncoding == TextEncoding::Ascii ?
                ui_->latinTextEdit : ui_->morseTextEdit;

    const bool isUserInput = source == editWithFocus_;
    if (!isUserInput)
        return;

    translationScheduler_->translate(source->toPlainText(), textEditEncoding);
}

void LiveMorseWindow::onTranslationReady(
        QString text, TextEncoding translationEncoding) {
    QTextEdit* targetEdit = translationEncoding == TextEncoding::Ascii ?
                ui_->latinTextEdit : ui_->morseTextEdit;

    targetEdit->setText(std::move(text));
}

void LiveMorseWindow::onLicenseTriggered(bool) {
    QFile file(":/license");
    if (!file.open(QIODevice::ReadOnly)) {
        Q_ASSERT(false);
    }
    QString licenseText{file.readAll()};
    file.close();

    auto* aboutModal = new QDialog(this);
    Ui::About dialog;
    dialog.setupUi(aboutModal);
    dialog.textEdit->setReadOnly(true);
    dialog.textEdit->setText(std::move(licenseText));
    file.close();
    aboutModal->exec();
}
