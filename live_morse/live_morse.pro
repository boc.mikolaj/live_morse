# This file is part of the Live Morse project distribution (https://gitlab.com/boc.mikolaj/live_morse/).
# Copyright (c) 2022 Mikolaj Boc.
#
# This file is part of the Live Morse project (LM).
#
# LM is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# LM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with LM. If not, see <https://www.gnu.org/licenses/>.

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    livemorsewindow.cpp

HEADERS += \
    livemorsewindow.h

FORMS += \
    dialog.ui \
    livemorsewindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../translation_engine -ltranslation_engine
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../translation_engine -ltranslation_engine
else:unix: LIBS += -L$$OUT_PWD/../translation_engine/ -ltranslation_engine

INCLUDEPATH += $$PWD/../translation_engine
DEPENDPATH += $$PWD/../translation_engine

RESOURCES += \
    resources.qrc
