/**
 * This file is part of the Live Morse project distribution (https://gitlab.com/boc.mikolaj/live_morse/).
 * Copyright (c) 2022 Mikolaj Boc.
 *
 * This file is part of the Live Morse project (LM).
 *
 * LM is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * LM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with LM. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIVEMORSEWINDOW_H
#define LIVEMORSEWINDOW_H

#include <memory>

#include <QMainWindow>

class QSignalMapper;
class QTextEdit;

enum class TextEncoding;
class TranslationScheduler;

QT_BEGIN_NAMESPACE
namespace Ui { class LiveMorseWindow; }
namespace Ui { class About; }
QT_END_NAMESPACE

// Main window for the LM application.
class LiveMorseWindow final : public QMainWindow {
    Q_OBJECT

 public:
    explicit LiveMorseWindow(QWidget *parent = nullptr);
    ~LiveMorseWindow() final;

 private:
    bool eventFilter(QObject *object, QEvent *event);

    std::unique_ptr<Ui::LiveMorseWindow> ui_;
    std::unique_ptr<Ui::About> about_;
    QTextEdit* editWithFocus_;
    QSignalMapper* signalMapper_;
    TranslationScheduler* translationScheduler_;

    void onTextChanged(TextEncoding type);

 private slots:
    void onTextChangedMapped(int param);
    void onTranslationReady(QString text, TextEncoding encoding);
    void onLicenseTriggered(bool);
};
#endif  // LIVEMORSEWINDOW_H
