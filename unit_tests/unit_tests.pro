# This file is part of the Live Morse project distribution (https://gitlab.com/boc.mikolaj/live_morse/).
# Copyright (c) 2022 Mikolaj Boc.
#
# This file is part of the Live Morse project (LM).
#
# LM is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# LM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with LM. If not, see <https://www.gnu.org/licenses/>.

QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_translationenginetest.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../translation_engine -ltranslation_engine
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../translation_engine -ltranslation_engine
else:unix: LIBS += -L$$OUT_PWD/../translation_engine/ -ltranslation_engine

INCLUDEPATH += $$PWD/../translation_engine
DEPENDPATH += $$PWD/../translation_engine
