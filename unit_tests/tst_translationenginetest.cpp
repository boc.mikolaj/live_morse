/**
 * This file is part of the Live Morse project distribution (https://gitlab.com/boc.mikolaj/live_morse/).
 * Copyright (c) 2022 Mikolaj Boc.
 *
 * This file is part of the Live Morse project (LM).
 *
 * LM is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * LM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with LM. If not, see <https://www.gnu.org/licenses/>.
 */

#include <QtTest>

#include "../translation_engine/translationengine.h"
#include "../translation_engine/translationenginetypes.h"

class TranslationEngineTest : public QObject
{
    Q_OBJECT

public:
    TranslationEngineTest();
    ~TranslationEngineTest();

private slots:
    void test_there_and_back();
    void test_collapses_whitespace_when_translating_from_ascii();
    void test_collapses_whitespace_when_translating_from_morse();
};

TranslationEngineTest::TranslationEngineTest() = default;

TranslationEngineTest::~TranslationEngineTest() = default;

void TranslationEngineTest::test_there_and_back()
{
    const QString from = "The quick brown fox jumps over the lazy dog 1234567890";
    const QString morseResult = translate(from, TextEncoding::Ascii);

    // Expected values taken off https://morsecode.world
    QCOMPARE(morseResult, QString(
                 QStringLiteral("- .... . | --.- ..- .. -.-. -.- | -... .-. --- .-- -. | ..-. --- -..- | .--- ..- -- .--. ... | --- ...- . .-. | - .... . | .-.. .- --.. -.-- | -.. --- --. | .---- ..--- ...-- ....- ..... -.... --... ---.. ----. -----")));

    const QString asciiResult = translate(morseResult, TextEncoding::Morse);
    // Case information is lost in translation, literally
    QCOMPARE(asciiResult, from.toLower());
}

void TranslationEngineTest::test_collapses_whitespace_when_translating_from_ascii()
{
    const QString from = " \n   \t The quick brown\tfox jumps over  \t\nthe lazy\ndog\t    ";
    const QString morseResult = translate(from, TextEncoding::Ascii);

    // Expected values taken off https://morsecode.world
    QCOMPARE(morseResult, QString(
                 QStringLiteral("- .... . | --.- ..- .. -.-. -.- | -... .-. --- .-- -. | ..-. --- -..- | .--- ..- -- .--. ... | --- ...- . .-. | - .... . | .-.. .- --.. -.-- | -.. --- --.")));
}

void TranslationEngineTest::test_collapses_whitespace_when_translating_from_morse()
{
    const QString from = "  .--   \n  \t .. -. -. .. . | - .... . | .--. --- ---  \t ....\n\n";
    const QString morseResult = translate(from, TextEncoding::Morse);

    // Expected values taken off https://morsecode.world
    QCOMPARE(morseResult, QString(
                 QStringLiteral("winnie the pooh")));
}

QTEST_APPLESS_MAIN(TranslationEngineTest)

#include "tst_translationenginetest.moc"
